var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var runSequence = require('run-sequence');
var notifier = require('node-notifier');
var addsrc = require('gulp-add-src');
var vinylPaths = require('vinyl-paths');
var del = require('del');
var zip = require('gulp-zip');
var order = require("gulp-order");
var print = require("gulp-print");
var concat = require("gulp-concat");
var replace = require('gulp-replace');
var dateFormat = require('dateformat');

// These are some common paths that we use throughout here.
var paths = {
    'build': './build',
    'bootstrap': './node_modules/bootstrap-sass/assets/',
    'jquery': './node_modules/jquery/dist/'
}

// This task generates the SASS files used everywhere on the site.
gulp.task('build:sass', function() {

    // These are some include paths that we might use in some of the files. This
    // allows us to, for instance, say @include "font-awesome" and have it
    // resolved into the file here when we build it.
    var includePaths = [
        paths.bootstrap + 'stylesheets/'
    ];

    return gulp.src("./scss/*.scss")
        .pipe(sass({
                outputStyle: 'compressed',
                includePaths: includePaths
            }).on('error', sass.logError))
        .pipe(gulp.dest('build'));
});

gulp.task('build:dependencies:js', function() {
    return gulp.src(paths.jquery + "jquery.js")
        .pipe(addsrc(paths.bootstrap + "javascripts/bootstrap.js"))
        .pipe(order(["**/jquery.js", "*"]))
        .pipe(concat('tviw.js'))
        .pipe(minify())
        .pipe(gulp.dest('build'));
});

gulp.task('build:src', function() {
    return gulp.src("./src/**")
        .pipe(replace("[[version]]", dateFormat(new Date(), "yyyymmddhhmmss")))
        .pipe(gulp.dest(paths.build));
});

gulp.task('build:vendor', function() {
    return gulp.src("./vendor/**")
        .pipe(gulp.dest(paths.build + '/vendor'));
});

gulp.task('watch', function() {
    runSequence('build', function() {
        gulp.watch("src/**", ['build:src']);
        gulp.watch("scss/*.scss", ['build:sass']);

        // Send a message when we get finished.
        gulp.on('task_stop', function (e) {
            notifier.notify({
                'title': 'Build Complete',
                'message': e.message
            });
        });
    });
});

gulp.task('clean', function() {
    return gulp.src("build/*")
        .pipe(addsrc("products/*"))
        .pipe(vinylPaths(del));
});

gulp.task('build', ['build:sass', 'build:src', 'build:vendor', 'build:dependencies:js']);

gulp.task('generate', ['build'], function() {
    return gulp.src("build/**")
        .pipe(zip("tviw-" + dateFormat(new Date(), "yyyymmddhhmmss") + ".zip"))
        .pipe(gulp.dest("products/"));
});

gulp.task('default', ['build'])
