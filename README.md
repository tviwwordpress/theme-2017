# TVIW 2017 Wordpress Theme

This is the TVIW 2017 Wordpress theme.

## Prerequisites

You'll need a working `npm`, `gulp` and `composer` to build the theme from
source. If you are going to do development, you will also need Vagrant and
VirtualBox.

## Building the Theme

1. Checkout the code: `git clone git@bitbucket.org:tviwwordpress/theme-2017.git`

1. `cd theme-2017`

1. `composer install`

1. `npm install`

1. `gulp generate`

This will build the site into a zip file that can be uploaded. Alternatively,
you can simply copy the contents of the `build` directory to the appropriate
location on the live Wordpress site.

## Installing

### Prerequisites

You will need the following plugins installed:

1. Gecka Submenu
1. Social Media Widget by Acurax
1. Bootstrap 3 Shortcodes

### Upload The Theme

Take the zip file generated above. Go to Appearance -> Themes. Click "Add New"
at the top. Click "Upload Theme". Select the zip file and upload it. Activate
it.

### Configure The Theme

This theme is **100% widgetized**. Everything, including the content of a page
is rendered by widgets.

1. Go to Appearance -> Customize.
1. Go to Site Identity. Select the logo and set the site title and tagline.
Click save.
1. Go to Appearance -> Widgets.
1. Drag "TVIW Content" to "Page Main Content Well"
1. Drag "TVIW Content" to "Post Main Content Well"
1. Drag "TVIW Content" to "Front Page Content Well"
1. Drag "TVIW Speakers" to "Front Page Content Well"
1. Drag "TVIW Sponsors" to "Front Page Content Well"
1. Drag "Custom Menu [advanced]" to "Page Side Content Well". Set it to use
"Menu 1 (Main Navigation Structure)" and "Current page's top parent."
1. Drag "Custom Menu [advanced]" to "Post Side Content Well". Set it to use
"Menu 1 (Main Navigation Structure)" and "Current page's top parent."
1. Drag "Acurax Social Media Widget" to "Footer Right"
1. Go to "Social Media Widget Settings". Select "Theme30". Enter our social
media accounts and click "Save".

## Development

If you are going to do theme development, this package includes VCCW, a
Wordpress Vagrant environment. Change to the `vccw` directory and run
`vagrant up`. You'll need to dump a copy of the site from the live environment
and upload it your VCCW environment once you're ready.

While doing development, you can run `gulp watch`, which will watch the PHP and
SCSS files for changes and regenerate the site when changes are found.
