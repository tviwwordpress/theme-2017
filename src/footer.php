<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php dynamic_sidebar('footer-left'); ?>
            </div>
            <div class="col-md-4">
                <?php dynamic_sidebar('footer-right'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 small text-center copyright">
                Copyright &copy; 2011 - <?php echo date("Y") ?> Tennessee Valley
                Interstellar Workshop. All rights reserved.
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
