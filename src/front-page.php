<?php get_header() ?>

<section class="hero">
    <?php dynamic_sidebar('front-page-hero'); ?>
</section>

<main>
    <div class="container">
        <?php dynamic_sidebar('front-page-content-well'); ?>
    </div>
</main>

<?php get_footer() ?>
