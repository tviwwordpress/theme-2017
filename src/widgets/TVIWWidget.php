<?php

class TVIWWidget extends WP_Widget {
    public function load_template($file, $instance = null) {
        include get_stylesheet_directory() . "/widgets/templates/$file.php";
    }
}
