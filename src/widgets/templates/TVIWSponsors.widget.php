<style>
.sponsors img {
    max-width: 200px;
    max-height: 200px;
}

.sponsors img:hover {
    -webkit-filter: none;
    filter: none;
}
</style>

<?php if (!empty($instance["title"])) { ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <h2><?php echo $instance["title"] ?></h2>
        </div>
    </div>
<?php } ?>

<div class="row sponsors">
    <?php for ($i = 1; $i < 5; $i++) { ?>
        <div class="col-md-3 text-center">
            <?php if (!filter_var($instance["link_$i"], FILTER_VALIDATE_URL) === false) { ?>
                <a href="<?= $instance["link_$i"] ?>" target="_blank">
            <?php } ?>

                <?php if (!filter_var($instance["image_$i"], FILTER_VALIDATE_URL) === false) { ?>
                    <!-- <img class="img-circle" src="" /> -->
                    <img class="" src="<?= $instance["image_$i"] ?>"  />
                <?php } ?>

                <?php if (!empty($instance["title_$i"])) { ?>
                    <h3><?php echo $instance["title_$i"] ?></h3>
                <?php } ?>

            <?php if (!filter_var($instance["link_$i"], FILTER_VALIDATE_URL) === false) { ?>
                </a>
            <?php } ?>
        </div>
    <?php } ?>
</div>
