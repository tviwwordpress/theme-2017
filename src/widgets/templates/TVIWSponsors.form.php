<?php

$title = "";
if (isset($instance["title"])) {
    $title = $instance["title"];
}

?>

<p>
<label for="<?php echo $this->get_field_name("title"); ?>"><?php _e("Title:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("title"); ?>"
    name="<?php echo $this->get_field_name("title"); ?>"
    type="text"
    value="<?php echo esc_attr($title); ?>" />
</p>

<?php for ($i = 1; $i < 5; $i++) {

    if (isset($instance["title_$i"])) {
        $image = $instance["image_$i"];
        $title = $instance["title_$i"];
        $link = $instance["link_$i"];
    } else {
        $title = __('Sponsor', 'text_domain');
        $image = __('', 'text_domain');
        $link = __('New URL', 'text_domain');
    }
?>
    <p>
        <label for="<?php echo $this->get_field_name("title_$i"); ?>"><?php _e("Sponsor $i Title:"); ?></label>
        <input
            class="widefat"
            id="<?php echo $this->get_field_id("title_$i"); ?>"
            name="<?php echo $this->get_field_name("title_$i"); ?>"
            type="text"
            value="<?php echo esc_attr($title); ?>" />
    </p>

    <p>
        <label for="<?php echo $this->get_field_name("link_$i"); ?>"><?php _e("Sponsor $i Link:"); ?></label>
        <input
            class="widefat"
            id="<?php echo $this->get_field_id("link_$i"); ?>"
            name="<?php echo $this->get_field_name("link_$i"); ?>"
            type="text"
            value="<?php echo esc_attr($link); ?>" />
    </p>

    <p>
        <label for="<?php echo $this->get_field_name("image_$i"); ?>"><?php _e("Sponsor $i Image:"); ?></label>
        <input
            class="widefat"
            id="<?php echo $this->get_field_id("image_$i"); ?>"
            name="<?php echo $this->get_field_name("image_$i"); ?>"
            type="text"
            value="<?php echo esc_attr($image); ?>" />

        <button class="tviw-sponsors-upload-btn button button-primary" data-field="<?php echo $this->get_field_id("image_$i"); ?>">Select Image</button>
    </p>
<?php } ?>
