<?php

$title = "";
if (isset($instance["title"])) {
    $title = $instance["title"];
}

?>

<p>
<label for="<?php echo $this->get_field_name("title"); ?>"><?php _e("Title:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("title"); ?>"
    name="<?php echo $this->get_field_name("title"); ?>"
    type="text"
    value="<?php echo esc_attr($title); ?>" />
</p>

<?php for ($i = 1; $i < 4; $i++) {

    if (isset($instance["speaker_$i"])) {
        $speaker = $instance["speaker_$i"];
        $image = $instance["image_$i"];
        $title = $instance["title_$i"];
    } else {
        $speaker = __('New Speaker', 'text_domain');
        $title = __('New Title', 'text_domain');
        $image =__('New Image', 'text_domain');
    }
?>
    <p>
    <label for="<?php echo $this->get_field_name("speaker_$i"); ?>"><?php _e("Speaker $i Name:"); ?></label>
    <input
        class="widefat"
        id="<?php echo $this->get_field_id("speaker_$i"); ?>"
        name="<?php echo $this->get_field_name("speaker_$i"); ?>"
        type="text"
        value="<?php echo esc_attr($speaker); ?>" />
    </p>

    <label for="<?php echo $this->get_field_name("title_$i"); ?>"><?php _e("Speaker $i Title:"); ?></label>
    <input
        class="widefat"
        id="<?php echo $this->get_field_id("title_$i"); ?>"
        name="<?php echo $this->get_field_name("title_$i"); ?>"
        type="text"
        value="<?php echo esc_attr($title); ?>" />
    </p>

    <label for="<?php echo $this->get_field_name("image_$i"); ?>"><?php _e("Speaker $i Image:"); ?></label>
    <input
        class="widefat"
        id="<?php echo $this->get_field_id("image_$i"); ?>"
        name="<?php echo $this->get_field_name("image_$i"); ?>"
        type="text"
        value="<?php echo esc_attr($image); ?>" />
    </p>

    <button class="upload_image_button button button-primary" data-field="<?php echo $this->get_field_id("image_$i"); ?>">Select Image</button>
<?php } ?>
