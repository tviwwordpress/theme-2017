<?php

if (isset($instance["image"])) {
    $image = $instance["image"];
    $position = $instance["position"];
    $large_height = $instance["large_height"];
    $medium_height = $instance["medium_height"];
    $small_height = $instance["small_height"];
    $xs_height = $instance["xs_height"];
} else {
    $image =__('New Image', 'text_domain');
    $position = "center";
    $large_height = 400;
    $medium_height = 300;
    $small_height = 200;
    $xs_height = 0;
}

?>

<label for="<?php echo $this->get_field_name("image"); ?>"><?php _e("Image:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("image"); ?>"
    name="<?php echo $this->get_field_name("image"); ?>"
    type="text"
    value="<?php echo esc_attr($image); ?>" />
</p>

<button class="cover-upload-button button button-primary" data-field="<?php echo $this->get_field_id("image"); ?>">Select Image</button>

<label for="<?php echo $this->get_field_name("large_height"); ?>"><?php _e("Large Height:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("large_height"); ?>"
    name="<?php echo $this->get_field_name("large_height"); ?>"
    type="text"
    value="<?php echo esc_attr($large_height); ?>" />
</p>

<label for="<?php echo $this->get_field_name("medium_height"); ?>"><?php _e("Medium Height:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("medium_height"); ?>"
    name="<?php echo $this->get_field_name("medium_height"); ?>"
    type="text"
    value="<?php echo esc_attr($medium_height); ?>" />
</p>

<label for="<?php echo $this->get_field_name("small_height"); ?>"><?php _e("Small Height:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("small_height"); ?>"
    name="<?php echo $this->get_field_name("small_height"); ?>"
    type="text"
    value="<?php echo esc_attr($small_height); ?>" />
</p>

<label for="<?php echo $this->get_field_name("xs_height"); ?>"><?php _e("XS Height:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("xs_height"); ?>"
    name="<?php echo $this->get_field_name("xs_height"); ?>"
    type="text"
    value="<?php echo esc_attr($xs_height); ?>" />
</p>

<label for="<?php echo $this->get_field_name("position"); ?>"><?php _e("Position:"); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id("position"); ?>"
    name="<?php echo $this->get_field_name("position"); ?>"
    type="text"
    value="<?php echo esc_attr($position); ?>" />
</p>
