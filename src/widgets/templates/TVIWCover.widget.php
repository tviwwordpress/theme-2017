<style>
.hero {
    background-image: url(<?=$instance["image"] ?>);
    background-position: <?=$instance["position"] ?>;
    background-size: cover;
}

@media screen and (min-width: 480px) {
    .hero {
        height: <?=$instance["xs_height"] ?>px;
    }
}

@media screen and (min-width: 768px) {
    .hero {
        height: <?=$instance["small_height"] ?>px;
    }
}

@media screen and (min-width: 992px) {
    .hero {
        height: <?=$instance["medium_height"] ?>px;
    }
}

@media screen and (min-width: 992px) {
    .hero {
        height: <?=$instance["large_height"] ?>px;
    }
}
</style>

<div class="hero"></div>
