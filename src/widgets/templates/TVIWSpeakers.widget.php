<style>
.speakers img {
    width: 200px;
    height: 200px;
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
}

.speakers img:hover {
    -webkit-filter: none;
    filter: none;
}
</style>

<?php if (!empty($instance["title"])) { ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <h2><?php echo $instance["title"] ?></h2>
        </div>
    </div>
<?php } ?>

<div class="row speakers">
    <?php for ($i = 1; $i < 4; $i++) { ?>
        <div class="col-md-4 text-center">

            <?php if (!filter_var($instance["image_$i"], FILTER_VALIDATE_URL) === false) { ?>
                <!-- <img class="img-circle" src="" /> -->
                <img class="img-circle img-thumbnail" src="<?= $instance["image_$i"] ?>"  />
            <?php } ?>

            <h3><?php echo $instance["speaker_$i"] ?></h3>
            <h5><?php echo $instance["title_$i"] ?></h5>
        </div>
    <?php } ?>
</div>
