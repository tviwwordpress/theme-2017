<?php

/**
 * This is the simple content widget.
 */
class TVIWCover extends TVIWWidget {
    /**
     * The constructor. Calls the parent to provide information about the
     * widget.
     */
    function __construct() {
        add_action('admin_enqueue_scripts', array($this, 'scripts'));

        parent::__construct(
            // Base ID of your widget
            'TVIWCover',

            // Widget name will appear in UI
            __('TVIW Cover Image', 'tviw_widget_domain'),

            // Widget description
            array('description' => __('A widget that generates the cover image', 'tviw_widget_domain')),

            null
        );
    }

    public function scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
        wp_enqueue_script('media-button', get_template_directory_uri() . '/admin/media-button.js', array('jquery'));
        wp_enqueue_script('TVIWCover', get_template_directory_uri() . '/admin/TVIWCover.js', array('jquery'));
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();

        $instance["image"] = ( ! empty( $new_instance["image"] ) ) ? $new_instance["image"] : '';
        $instance["position"] = ( ! empty( $new_instance["position"] ) ) ? $new_instance["position"] : 'center';
        $instance["large_height"] = ( ! empty( $new_instance["large_height"] ) ) ? $new_instance["large_height"] : 0;
        $instance["medium_height"] = ( ! empty( $new_instance["medium_height"] ) ) ? $new_instance["medium_height"] : 0;
        $instance["small_height"] = ( ! empty( $new_instance["small_height"] ) ) ? $new_instance["small_height"] : 0;
        $instance["xs_height"] = ( ! empty( $new_instance["xs_height"] ) ) ? $new_instance["xs_height"] : 0;

        return $instance;
    }

    /**
     * Generates the widget.
     *
     * @param  array $args     Arguments passed in.
     * @param  id $instance An instance of the widget.
     * @return void
     */
    public function widget($args, $instance) {
        $this->load_template("TVIWCover.widget", $instance, $args);
    }

    public function form( $instance ) {
        $this->load_template("TVIWCover.form", $instance);
    }
}
