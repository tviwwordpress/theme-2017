<?php

/**
 * This is the simple content widget.
 */
class TVIWSpeakers extends TVIWWidget {
    /**
     * The constructor. Calls the parent to provide information about the
     * widget.
     */
    function __construct() {
        add_action('admin_enqueue_scripts', array($this, 'scripts'));

        parent::__construct(
            // Base ID of your widget
            'TVIWSpeakers',

            // Widget name will appear in UI
            __('TVIW Speakers', 'tviw_widget_domain'),

            // Widget description
            array('description' => __('A widget that generates the speakers box', 'tviw_widget_domain')),

            null
        );
    }

    public function scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
        wp_enqueue_script('media-button', get_template_directory_uri() . '/admin/media-button.js', array('jquery'));
        wp_enqueue_script('TVIWSpeakers', get_template_directory_uri() . '/admin/TVIWSpeakers.js', array('jquery'));
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();

        $instance["title"] = ( ! empty( $new_instance["title"] ) ) ? strip_tags( $new_instance["title"] ) : '';
        for ($i = 1; $i < 4; $i++) {
            $instance["speaker_$i"] = ( ! empty( $new_instance["speaker_$i"] ) ) ? strip_tags( $new_instance["speaker_$i"] ) : '';
            $instance["title_$i"] = ( ! empty( $new_instance["title_$i"] ) ) ? strip_tags( $new_instance["title_$i"] ) : '';
            $instance["image_$i"] = ( ! empty( $new_instance["image_$i"] ) ) ? $new_instance["image_$i"] : '';
        }

        return $instance;
    }

    /**
     * Generates the widget.
     *
     * @param  array $args     Arguments passed in.
     * @param  id $instance An instance of the widget.
     * @return void
     */
    public function widget($args, $instance) {
        $this->load_template("TVIWSpeakers.widget", $instance, $args);
    }

    public function form( $instance ) {
        $this->load_template("TVIWSpeakers.form", $instance);
    }
}
