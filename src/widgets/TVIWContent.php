<?php

/**
 * This is the simple content widget.
 */
class TVIWContent extends TVIWWidget {
    /**
     * The constructor. Calls the parent to provide information about the
     * widget.
     */
    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'TVIWContent',

            // Widget name will appear in UI
            __('TVIW Content', 'tviw_widget_domain'),

            // Widget description
            array('description' => __('A widget that generates the page\'s content.', 'tviw_widget_domain')),

            null
        );
    }

    /**
     * Generates the widget.
     *
     * @param  array $args     Arguments passed in.
     * @param  id $instance An instance of the widget.
     * @return void
     */
    public function widget($args, $instance) {
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_content();
            }
        }
    }
}
