<?php

require "vendor/autoload.php";

use Salaros\Wordpress\wp_bootstrap_navwalker;

wp_enqueue_style('tviw', get_template_directory_uri() . '/tviw.css');
wp_enqueue_script('tviw-min', get_template_directory_uri() . '/tviw-min.js');

?>
<html <?php language_attributes(); ?>>
<head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>
</head>

<body>
    <?php show_admin_bar(true); ?>
    <nav class="navbar navbar-default">
        <div class="container">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle Nav</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <?php the_custom_logo(); ?>

                <div class="navbar-brand hidden-md hidden-sm hidden-xs">
                    <?php echo get_bloginfo("description") ?>
                </div>


            <?php
                wp_nav_menu([
                    'theme_location'    => 'top',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse menu-bar',
                    'container_id'      => 'bs-example-navbar-collapse-1',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker()
                ]);
            ?>
        </div>
    </nav>
