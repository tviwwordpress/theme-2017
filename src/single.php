<?php get_header() ?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-push-3 col-md-9">
                <?php dynamic_sidebar('post-main-content-well'); ?>
            </div>

            <div class="col-md-pull-9 col-md-3">
                <?php dynamic_sidebar('post-side-content-well'); ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer() ?>
