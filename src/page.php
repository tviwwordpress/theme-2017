<?php get_header() ?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-3">
                <h1><?php single_post_title(); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-push-3 col-md-9 main">
                <?php dynamic_sidebar('page-main-content-well'); ?>
            </div>

            <div class="col-md-pull-9 col-md-3 sidebar">
                <?php dynamic_sidebar('page-side-content-well'); ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer() ?>
