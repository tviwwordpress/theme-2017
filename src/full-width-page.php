<?php

/* Template Name: Full Width Page */

get_header();

?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php single_post_title(); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 main">
                <?php dynamic_sidebar('page-main-content-well'); ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer() ?>
