<?php

/* Template Name: Schedule Page */

get_header();

$query = new WP_Query(array(
    'post_type' => 'panel',
    'post_status' => 'publish'
));

$panels = [];

while ($query->have_posts()) {
    $query->the_post();
    $panel = get_post();

    $metadata = get_post_meta($panel->ID);
    $panel->metadata = $metadata;

    $start = date("l, F j, Y", $panel->metadata["start_ts"][0]);

    $panels[$start][] = $panel;

}


wp_enqueue_style('schedule', get_stylesheet_directory_uri() . "/schedule.css");
wp_enqueue_script('tviw-schedule', get_template_directory_uri() . '/js/schedule.js');

?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php single_post_title(); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 main">
                <div class="cd-schedule loading">
                	<div class="timeline">
                		<ul>
                			<li><span>07:00</span></li>
                			<li><span>07:30</span></li>
                			<li><span>08:00</span></li>
                			<li><span>08:30</span></li>
                			<li><span>09:00</span></li>
                			<li><span>09:30</span></li>
                			<li><span>10:00</span></li>
                			<li><span>10:30</span></li>
                			<li><span>11:00</span></li>
                			<li><span>11:30</span></li>
                			<li><span>12:00</span></li>
                			<li><span>12:30</span></li>
                			<li><span>13:00</span></li>
                			<li><span>13:30</span></li>
                			<li><span>14:00</span></li>
                			<li><span>14:30</span></li>
                			<li><span>15:00</span></li>
                			<li><span>15:30</span></li>
                			<li><span>16:00</span></li>
                			<li><span>16:30</span></li>
                			<li><span>17:00</span></li>
                			<li><span>17:30</span></li>
                			<li><span>18:00</span></li>
                			<li><span>18:30</span></li>
                			<li><span>19:00</span></li>
                			<li><span>19:30</span></li>
                			<li><span>20:00</span></li>
                		</ul>
                	</div> <!-- .timeline -->

                    <div class="events">
                		<ul>
                            <?php foreach ($panels as $key => $inner) { ?>
                			<li class="events-group">
                				<div class="top-info"><span><?= $key ?></span></div>

                				<ul>
                                    <?php foreach ($inner as $panel) { ?>
                                    <li class="single-event" data-start="<?= $panel->metadata["start_time"][0] ?? "00:00" ?>" data-end="<?= $panel->metadata["end_time"][0] ?? "00:00" ?>" data-content="<?= $panel->ID ?>" data-event="event-<?= $panel->ID ?>">
                                        <a href="#0">
                                            <em class="event-name"><?= $panel->post_title ?></em>
                                            <div class="event-details"><?= get_the_excerpt($panel) ?></div>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="event-modal">
                        <header class="header">
                            <div class="content">
                                <span class="event-date"></span>
                                <h3 class="event-name"></h3>
                            </div>

                            <div class="header-bg"></div>
                        </header>

                        <div class="body">
                            <div class="event-info"></div>
                            <div class="body-bg"></div>
                        </div>

                        <a href="#0" class="close">Close</a>
                    </div>

                    <div class="cover-layer"></div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer() ?>
