<?php

include "widgets/TVIWWidget.php";
include "widgets/TVIWContent.php";
include "widgets/TVIWSpeakers.php";
include "widgets/TVIWCover.php";
include "widgets/TVIWSponsors.php";

register_sidebar([
    'name' => 'Footer Left',
    'id'   => 'footer-left'
]);

register_sidebar([
    'name' => 'Footer Right',
    'id'   => 'footer-right'
]);

register_sidebar([
    'name' => 'Front Page Hero',
    'id'   => 'front-page-hero'
]);

register_sidebar([
    'name' => 'Front Page Content Well',
    'id'   => 'front-page-content-well'
]);

register_sidebar([
    'name' => 'Page Main Content Well',
    'id'   => 'page-main-content-well'
]);

register_sidebar([
    'name' => 'Page Side Content Well',
    'id'   => 'page-side-content-well'
]);

register_sidebar([
    'name' => 'Post Main Content Well',
    'id'   => 'post-main-content-well'
]);

register_sidebar([
    'name' => 'Post Side Content Well',
    'id'   => 'post-side-content-well'
]);

add_theme_support('custom-logo');
add_theme_support('title-tag');

add_action('widgets_init', function() {
    register_widget('TVIWContent');
    register_widget('TVIWSpeakers');
    register_widget('TVIWCover');
    register_widget('TVIWSponsors');
});

add_action('after_setup_theme', function() {
    register_nav_menu('top', __( 'Top Navigation', 'top-nav' ) );
});

add_action('init', function() {
    register_post_type('panel', [
        'labels' => [
            'name' => __('Panels'),
            'singular_name' => __('Panel'),
            'add_new_item' => __('Add New Panel'),
            'edit_item' => __('Edit Panel')
        ],
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'rest_base' => 'panels',
        'rewrite' => array(
            'slug' => 'panels',
            'with_front' => false
        ),
    ]);
});

add_action( 'rest_api_init', function() {
    $get_meta_field = function($object, $field_name, $request) {
        $meta = get_post_meta($object['id'], $field_name);

        if (is_array($meta)) {
            $meta = array_shift($meta);
        }

        return $meta;
    };

    register_rest_field('panel', 'start_date', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'start_time', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'start_datetime', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'start_ts', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'end_date', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'end_time', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'end_datetime', ['get_callback' => $get_meta_field]);
    register_rest_field('panel', 'end_ts', ['get_callback' => $get_meta_field]);
});

add_action("admin_init", function() {
    add_meta_box("panel-meta", "Panel Options", function() {
        global $post;

        $custom = [];
        if (!empty($post)) {
            $custom = get_post_custom($post->ID);
        }

        ?>
        <strong>Start Date/Time:</strong>
        <input type="date" name="start_date" value="<?=$custom["start_date"][0] ?? 0 ?>" required />
        <input type="time" name="start_time" value="<?=$custom["start_time"][0] ?? 0 ?>" required /><br />
        <br />

        <strong>End Date/Time:</strong>
        <input type="date" name="end_date" value="<?=$custom["end_date"][0] ?? 0 ?>" required />
        <input type="time" name="end_time" value="<?=$custom["end_time"][0] ?? 0 ?>" required />
        <?php
    }, "panel", "side", "high");

    add_action('save_post', function() {
        global $post;

        if (!empty($post) && $post->post_type == "panel") {
            $start_datetime = $_POST["start_date"] . " " . $_POST["start_time"];
            $end_datetime = $_POST["end_date"] . " " . $_POST["end_time"];
            $start_ts = strtotime($start_datetime);
            $end_ts = strtotime($end_datetime);

            update_post_meta($post->ID, "start_date", $_POST["start_date"]);
            update_post_meta($post->ID, "start_time", $_POST["start_time"]);
            update_post_meta($post->ID, "end_date", $_POST["end_date"]);
            update_post_meta($post->ID, "end_time", $_POST["end_time"]);
            update_post_meta($post->ID, "start_datetime", $start_datetime);
            update_post_meta($post->ID, "end_datetime", $end_datetime);
            update_post_meta($post->ID, "start_ts", $start_ts);
            update_post_meta($post->ID, "end_ts", $end_ts);
        }
    });
});
